<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="errorPage.jsp"%>
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache" /> 
    <meta http-equiv="Cache-control" content="no-store"> 
    <meta http-equiv="Window-Target" content="_top">
	<link rel="stylesheet" href="css/structure.css"/>  
	<link rel="stylesheet" href="css/reset.css"/>
	<link rel="stylesheet" href="css/animate.css"/>
	<link rel="stylesheet" href="css/styles.css"/> 
	<script type="text/javascript" src = "scripts/jquery.js"></script>
	<script type="text/javascript" src = "scripts/jquery.js"></script>
	<script type="text/javascript" src = "scripts/jquery-ui.js"></script>
	<title>Logout</title>  
	
</head>
<body>
<div id="container">
<div style='position: fixed;padding-top: 70px;' align="center">
<label style='align:center;color:#0f0f0f'><!-- <h2>Thanks for using XDATA!</h2>-->	
<h2>You have logged out of XDATA</h2><br>
<h3> 
Please use Course Menu in the Learning tool(Moodle, Blackboard, Canvas, etc.,) to login again.</label>
</h3>
</div>
</div>
</body>
</html>